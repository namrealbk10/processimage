
import cv2
import ColorHistogram as CH
import LBP
import ImageDescriptor as IDes
import os
import numpy as np
os.system("cls")
import Contract
import threading
from sklearn.svm import SVC
from sklearn.datasets import load_digits
from sklearn.linear_model import SGDClassifier
import joblib

class VarStatic:
    numbImage = 10
    Apple = []
    labelApple = ["Apple"]*numbImage
    Banana = []
    labelBanana = ["Banana"]*numbImage
    Tomato = []
    labelTomato = ["Tomato"]*numbImage
def funsApple():
    VarStatic.Apple = Contract.loadMoreFeatured('Apple', VarStatic.numbImage)
    print("A")
def funsBanana():
    VarStatic.Banana = Contract.loadMoreFeatured('Banana', VarStatic.numbImage)
    print("B")
def funsTomato():
    VarStatic.Tomato = Contract.loadMoreFeatured('Tomato', VarStatic.numbImage)
    print("T")

# t1 = threading.Thread(target=funsApple, args=[])
# t2 = threading.Thread(target=funsBanana, args=[])
# t3 = threading.Thread(target=funsTomato, args=[])
# t1.start()
# t2.start()
# t3.start()
# t1.join()
# t2.join()
# t3.join()
def run():
    funsApple()
    funsBanana()
    funsTomato()
    X = np.array(VarStatic.Apple + VarStatic.Banana + VarStatic.Tomato)
    y = VarStatic.labelApple + VarStatic.labelBanana + VarStatic.labelTomato
    clf = SVC(kernel='linear', C=1E10)
    clf.fit(X, y)
    joblib.dump(clf, 'SVM_DATA.pkl')


# digits = load_digits()
# clf = SGDClassifier().fit(digits.data, digits.target)
# print(clf.score(digits.data, digits.target))