import numpy as np
import convolute_lib as cnn
import convert
import cv2
from scipy import ndimage
# Bộ dò biên Canny

################################ b1:  Smoothing: Gaussian
    # Gaussian mask sigma = 1.4
# gaussianMask = (1/159)*np.array([   
#                     [ 2,  4,  5,  4,  2], 
#                     [ 4,  9, 12,  9,  4], 
#                     [ 5, 12, 15, 12,  5], 
#                     [ 4,  9, 12,  9,  4], 
#                     [ 2,  4,  5,  4,  2]
#                 ])
gaussianMask = (1/273)*np.array([   
                    [ 1,  4,  7, 4,   1], 
                    [ 4, 16, 26, 16,  4], 
                    [ 7, 26, 41, 26,  7], 
                    [ 4, 16, 26, 16,  4], 
                    [ 1,  4,  7,  4,  1]
                ])


def imgSmoothing(img):
    out = cnn.convolve_np4(img, gaussianMask)   # float
    return convert.convertToUint8(out)

########################################################## b2:  Tinh Gradient   
    # Wx mask sobel kernel theo x ( dao ham theo phuong x)
Wx = np.array([   
                    [ -1, 0, 1], 
                    [ -2, 0, 2],
                    [ -1, 0, 1]
                ])
    # Wy sobel kernel theo y ( dao ham theo phuong y)
Wy = np.array([   
                    [ -1, -2, -1], 
                    [  0,  0,  0],
                    [  1,  2,  1]
                ])
def imgGradient(img):
    img_height = img.shape[0]
    img_width = img.shape[1]            
    #  Gx Gradient theo x
    Gx = cnn.convolve_np4(img, Wx)
    #  Gy Gradient theo y
    Gy = cnn.convolve_np4(img, Wy)
    # Tinh SUM = |Gx| + |Gy|
    Sum = np.zeros((img_height, img_width))
    # Tinh Angle Gx/Gy
    Angle = np.zeros((img_height, img_width))
    # 
    Sum  = np.abs(Gx) + np.abs(Gy)  # |Gx| + |Gy|
    Angle = np.arctan2(Gy, Gx) * 180 / np.pi  # arctan(Gy/Gx)  
    Angle = np.abs(Angle)
    Angle[Angle <= 22.5] = 0
    Angle[Angle >= 157.5] = 0
    Angle[(Angle > 22.5) * (Angle < 67.5)] = 45
    Angle[(Angle >= 67.5) * (Angle <= 112.5)] = 90
    Angle[(Angle > 112.5) * (Angle <= 157.5)] = 135
                        
    return [Sum, Angle]

#############################  B3: loai bo 1 số điểm

def kindOfPixel(imgSmoothing, gradientSum, gradientAngle):
    img_height = imgSmoothing.shape[0]
    img_width  = imgSmoothing.shape[1]
    keep_mask = np.zeros(imgSmoothing.shape, np.uint8)
    for y in range(1, img_height-1):
        for x in range(1, img_width-1):
            area_grad_intensity = gradientSum[y-1:y+2, x-1:x+2] # 3x3 area
            area_angle = gradientAngle[y-1:y+2, x-1:x+2] # 3x3 area
            current_angle = area_angle[1,1]
            current_grad_intensity = area_grad_intensity[1,1]
            
            if current_angle == 0:
                if current_grad_intensity > max(area_grad_intensity[1,0], area_grad_intensity[1,2]):
                    keep_mask[y,x] = 255
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 45:
                if current_grad_intensity > max(area_grad_intensity[2,0], area_grad_intensity[0,2]):
                    keep_mask[y,x] = 255
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 90:
                if current_grad_intensity > max(area_grad_intensity[0,1], area_grad_intensity[2,1]):
                    keep_mask[y,x] = 255
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 135:
                if current_grad_intensity > max(area_grad_intensity[0,0], area_grad_intensity[2,2]):
                    keep_mask[y,x] = 255
                # else:
                #     gradientSum[y,x] = 0

    return [keep_mask, gradientSum]

def chooseScope(keep_mask, gradientSum, TH, TL):
    outResult = np.zeros(keep_mask.shape, np.uint8)
    img_height = keep_mask.shape[0]
    img_width  = keep_mask.shape[1]
    ###### B4: lay nguong Th Tl
    #TH1 : >TH 
    outResult[(keep_mask>0)*(gradientSum>=TH)] = 255
    #TH2: <TL
    outResult[(keep_mask>0)*(gradientSum<TL)] = 0
    #TH3: TL<...<TH
    for y in range(1, img_height-1):
        for x in range(1, img_width-1):
            if  outResult[y, x] and gradientSum[y, x] > TL and gradientSum[y, x] < TH:
                area_3x3 = gradientSum[y-1:y+2, x-1:x+2] # 3x3 area
                center_area_3x3 = area_3x3[1,1]        # gradientSum[x, y]
                maxarea = max(area_3x3[0,0], area_3x3[0,1], area_3x3[0,2], area_3x3[1,0], area_3x3[1,2], area_3x3[2,0], area_3x3[2,1], area_3x3[2,2] )
                if maxarea >= TH:
                    outResult[y, x] = 255          
    return outResult

# B5: rut chinh dac trung
# tong hop 5 buoc
def resultImageDescriptor(image):
    imgGray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    imgS = imgSmoothing(imgGray)
    # imgS = cv2.GaussianBlur(imgGray, ksize=(5, 5), sigmaX=1, sigmaY=1)
    gradientSum, gradientAngle  = imgGradient(imgS)
    keep_mask, gradientSum = kindOfPixel(imgS, gradientSum, gradientAngle)
    choSc = chooseScope(keep_mask, gradientSum, 200, 100)    
    return choSc
# nhận xét: sau 5 bước thì chỉ có lấy rõ dc biện trên
# ta sẽ tiến hành xoay ảnh rồi gộp 2 biên trên dưới
def collageWithRotate(image):
    # xoay ảnh
    tmpRotate = ndimage.rotate(image, 180, reshape=False)
    tmpRotate[tmpRotate==0] = 255
    # tìm biên
    outResult = resultImageDescriptor(image)
    tmpRotate = resultImageDescriptor(tmpRotate)
    # xoay lại
    tmpRotate = ndimage.rotate(tmpRotate, 180, reshape=False)
    # ghép biên
    outResult[(outResult==0)*(tmpRotate>0)] = 255
    # xóa điểm ảnh nhiễu
    for y in range(1, outResult.shape[0]-1):
        for x in range(1, outResult.shape[1]-1):
            area_3x3 = outResult[y-1:y+2, x-1:x+2] # 3x3 area
            maxarea = max(area_3x3[0,0], area_3x3[0,1], area_3x3[0,2], area_3x3[1,0], area_3x3[1,2], area_3x3[2,0], area_3x3[2,1], area_3x3[2,2] )
            if maxarea == 0:
                outResult[y, x] = 0       
    return outResult

# trích chọn 7 đặc trưng Hu Moments
def moment_pq(image, p, q):
    outResult = np.int64(0)
    for x in range(0, image.shape[0]-1):
        for y in range(0, image.shape[1]-1):
            if image[x, y] > 0:
                outResult += (x**p)*(y**q)*image[x, y]
    # chuẩn hóa for p+q >=2
    if p + q >= 2:
        M00 = sum(sum(image))
        y = (p + q)//2 + 1
        outResult = outResult/(M00**y)           
    return outResult

def fd_hu_moments(image):
    outResult = np.zeros(7) # 7 đặc trưng Hu Moments
    m20 = moment_pq(image, 2, 0)
    m02 = moment_pq(image, 0, 2)
    m11 = moment_pq(image, 1, 1)
    m12 = moment_pq(image, 1, 2)
    m21 = moment_pq(image, 2, 1)
    m30 = moment_pq(image, 3, 0)
    m03 = moment_pq(image, 0, 3)
    # đặc trưng 1:
    outResult[0] = m20 + m02
    # đặc trưng 2:
    outResult[1] = (m20 - m02)**2 + 4*m11**2
    # đặc trưng 3:
    outResult[2] = (m30 - 3*m12)**2 + (3*m21 - m03)**2
    # đặc trưng 4:
    outResult[3] = (m30 + m12)**2 + (m21 + m03)**2
    # đặc trưng 5:
    outResult[4] = (m30 - 3*m12)*(m30 + m12)*((m30 + m12)**2 -3*(m03 + m21)**2) + (3*m21 - m03)*(m21 + m03)*( 3*(m30 + m12)**2 - (m21 + m03)**2 )
    # đặc trưng 6:
    outResult[5] = (m20 - m02)*( (m30 + m12)**2 - (m21 + m03)**2 ) + 4*m11*(m30 + m12)*(m21 + m03)
    # đặc trưng 7:
    outResult[6] = (3*m21 - m03)*(m30 + m12)*( (m30 + m12)**2 - 3*(m21 + m03)**2) - (m30 -3*m12)*(m21 + m03)*( 3*(m30 + m12)**2 - (m21 + m03)**2)
    #
    return outResult


