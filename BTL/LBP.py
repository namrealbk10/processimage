import numpy as np
  
# chuan hoa
def funStandardized(data, sumValues):
    length = len(data)                      # length
    values = np.zeros(length)               # init return value
    for i in range(length):
        values[i] = data[i] / sumValues        # chia cho length
    return values

def funBinTo2(bin):
    return bin[0]*128 + bin[1]*64 + bin[2]*32 + bin[3]*16 + bin[4]*8 + bin[5]*4 + bin[6]*2 + bin[7]

def funParternOfPixel(data):
    height, width = data.shape[:2]
    values = np.zeros((height, width))
    bin = np.zeros(8)
    height = height -1
    width  = width -1 
    # duyet
    for i in range(1, height):
        for j in range(1, width):
            # bin[0] 
            if data[i, j] < data[i, j-1]:
                bin[0] = 0
            else:
                bin[0] = 1
            # bin[1] 
            if data[i, j] < data[i-1, j-1]:
                bin[1] = 0
            else:
                bin[1] = 1
            # bin[2] 
            if data[i, j] < data[i-1, j]:
                bin[2] = 0
            else:
                bin[2] = 1
            # bin[3] 
            if data[i, j] < data[i-1, j+1]:
                bin[3] = 0
            else:
                bin[3] = 1
            # bin[4] 
            if data[i, j] < data[i, j+1]:
                bin[4] = 0
            else:
                bin[4] = 1    
            # bin[5] 
            if data[i, j] < data[i+1, j+1]:
                bin[5] = 0
            else:
                bin[5] = 1    
            # bin[6] 
            if data[i, j] < data[i+1, j]:
                bin[6] = 0
            else:
                bin[6] = 1
            # bin[7] 
            if data[i, j] < data[i+1, j-1]:
                bin[7] = 0
            else:
                bin[7] = 1

            values[i, j] = funBinTo2(bin) 
    # voi i = 0 , i = height 
    for j in range(1, width-1):
        # voi i = 0
        # bin[0] 
        if data[0, j] < data[0, j-1]:
            bin[0] = 0
        else:
            bin[0] = 1
        # bin[1] 
        bin[1] = 0
        # bin[2] 
        bin[2] = 0
        # bin[3] 
        bin[3] = 0
        # bin[4] 
        if data[0, j] < data[0, j+1]:
            bin[4] = 0
        else:
            bin[4] = 1    
        # bin[5] 
        if data[0, j] < data[1, j+1]:
            bin[5] = 0
        else:
            bin[5] = 1    
        # bin[6] 
        if data[0, j] < data[1, j]:
            bin[6] = 0
        else:
            bin[6] = 1
        # bin[7] 
        if data[0, j] < data[1, j-1]:
            bin[7] = 0
        else:
            bin[7] = 1
        values[0, j] = funBinTo2(bin) 
        # voi i = height
            # bin[0] 
        if data[height, j] < data[height, j-1]:
            bin[0] = 0
        else:
            bin[0] = 1
        # bin[1] 
        if data[height, j] < data[height-1, j-1]:
            bin[1] = 0
        else:
            bin[1] = 1
        # bin[2] 
        if data[height, j] < data[height-1, j]:
            bin[2] = 0
        else:
            bin[2] = 1
        # bin[3] 
        if data[height, j] < data[height-1, j+1]:
            bin[3] = 0
        else:
            bin[3] = 1
        # bin[4] 
        if data[height, j] < data[height, j+1]:
            bin[4] = 0
        else:
            bin[4] = 1    
        # bin[5] 
        bin[5] = 0  
        # bin[6] 
        bin[6] = 0
        # bin[7] 
        bin[7] = 0
        values[height, j] = funBinTo2(bin)
    # voi j = 0 , j = width 
    for i in range(1, height-1):
        # voi j = 0
        # bin[0] 
        bin[0] = 0
        # bin[1] 
        bin[1] = 0
        # bin[2] 
        if data[i, 0] < data[i-1, 0]:
            bin[2] = 0
        else:
            bin[2] = 1
        # bin[3] 
        if data[i, 0] < data[i-1, 1]:
            bin[3] = 0
        else:
            bin[3] = 1
        # bin[4] 
        if data[i, 0] < data[i, 1]:
            bin[4] = 0
        else:
            bin[4] = 1    
        # bin[5] 
        if data[i, 0] < data[i+1, 1]:
            bin[5] = 0
        else:
            bin[5] = 1    
        # bin[6] 
        if data[i, 0] < data[i+1, 0]:
            bin[6] = 0
        else:
            bin[6] = 1
        # bin[7] 
        bin[7] = 0
        values[i, 0] = funBinTo2(bin) 
        # voi j = width
        # bin[0] 
        if data[i, width] < data[i, width-1]:
            bin[0] = 0
        else:
            bin[0] = 1
        # bin[1] 
        if data[i, width] < data[i-1, width-1]:
            bin[1] = 0
        else:
            bin[1] = 1
        # bin[2] 
        if data[i, width] < data[i-1, width]:
            bin[2] = 0
        else:
            bin[2] = 1
        # bin[3] 
        bin[3] = 0
        # bin[4] 
        bin[4] = 0   
        # bin[5] 
        bin[5] = 0 
        # bin[6] 
        if data[i, width] < data[i+1, width]:
            bin[6] = 0
        else:
            bin[6] = 1
        # bin[7] 
        if data[i, width] < data[i+1, width-1]:
            bin[7] = 0
        else:
            bin[7] = 1

        values[i, width] = funBinTo2(bin)

    return values

def funFindiUniformPattern():
    values = np.zeros(256)
    stt = 0                # # LBP feature là 59: 0->58
    isBinFontEq1 = False
    isBinAfterEq1 = False
    count = 0
    for patterns in range(256):
        count = 0
        for i in range(7): # 01, 12, ,...,
            # check BinFont is equal == 0
            if 2**i & patterns == 0:
                isBinFontEq1 = False
            else:
                isBinFontEq1 = True
            # check BinAfter is equal == 0
            if 2**(i + 1) & patterns == 0:
                isBinAfterEq1 = False
            else:
                isBinAfterEq1 = True
            if isBinAfterEq1 != isBinFontEq1:
                count += 1

            if count == 3:
                break    
        # check count > 2 ==> non-uniform pattern        
        if count <= 2:
            values[patterns] = stt # uniform pattern  0->57
            stt +=1
        else:
            values[patterns] = 58  # non uniform pattern
    return values

def funGetLBPImage(dataImg):
    height, width = dataImg.shape[:2]
    patternImg = funParternOfPixel(dataImg)         # pattern Histogram
    listUniformPattern = funFindiUniformPattern()   # index of list is pattern 0->255
                                                    # value la index  uniform pattern 0->57 or 58 la  non uniform pattern
    # B1
    valueLBP =  np.zeros(59)                        # return value
    tmpPattern = 0
    tmpUniformPattern = 0
    for i in range(height):
        for j in range(width):
            tmpPattern = int(patternImg[i, j])
            tmpUniformPattern = int(listUniformPattern[tmpPattern]) # index uniformPattern in valueLBP
            valueLBP[tmpUniformPattern] += 1

    # B2: chuan hoa

    return funStandardized(valueLBP, height*width)
