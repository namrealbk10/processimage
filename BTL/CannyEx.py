import numpy as np
import convolute_lib as cnn
import convert
import cv2
# Bộ dò biên Canny

################################ b1:  Smoothing: Gaussian
    # Gaussian mask sigma = 1.4
gaussianMask = (1/159)*np.array([   
                    [ 2,  4,  5,  4,  2], 
                    [ 4,  9, 12,  9,  4], 
                    [ 5, 12, 15, 12,  5], 
                    [ 4,  9, 12,  9,  4], 
                    [ 2,  4,  5,  4,  2]
                ])

def imgSmoothing(img):
    out = cnn.convolve_np4(img, gaussianMask)   # float
    return convert.convertToUint8(out)

########################################################## b2:  Tinh Gradient   
    # Wx mask sobel kernel theo x ( dao ham theo phuong x)
Wx = np.array([   
                    [ -1, 0, 1], 
                    [ -2, 0, 2],
                    [ -1, 0, 1]
                ])
    # Wy sobel kernel theo y ( dao ham theo phuong y)
Wy = np.array([   
                    [ -1, -2, -1], 
                    [  0,  0,  0],
                    [  1,  2,  1]
                ])
def imgGradient(img):
    img_height = img.shape[0]
    img_width = img.shape[1]            
    #  Gx Gradient theo x
    Gx = cnn.convolve_np4(img, Wx)
    #  Gy Gradient theo y
    Gy = cnn.convolve_np4(img, Wy)
    # Tinh SUM = |Gx| + |Gy|
    Sum = np.zeros((img_height, img_width))
    # Tinh Angle Gx/Gy
    Angle = np.zeros((img_height, img_width))
    # 
    Sum  = np.abs(Gx) + np.abs(Gy)  # |Gx| + |Gy|
    Angle = np.arctan2(Gy, Gx) * 180 / np.pi  # arctan(Gy/Gx)  
    # Angle = np.abs(Angle)
    Angle[Angle < 0] += 180
    Angle[Angle <= 22.5] = 0
    Angle[Angle >= 157.5] = 0
    Angle[(Angle > 22.5) * (Angle < 67.5)] = 45
    Angle[(Angle >= 67.5) * (Angle <= 112.5)] = 90
    Angle[(Angle > 112.5) * (Angle <= 157.5)] = 135
                        
    return [Sum, Angle]

#############################  B3: loai bo 1 số điểm

def kindOfPixel(imgSmoothing, gradientSum, gradientAngle):
    img_height = imgSmoothing.shape[0]
    img_width  = imgSmoothing.shape[1]
    keep_mask = np.zeros(imgSmoothing.shape, np.uint8)
    for y in range(1, img_height-1):
        for x in range(1, img_width-1):
            area_grad_intensity = gradientSum[y-1:y+2, x-1:x+2] # 3x3 area
            area_angle = gradientAngle[y-1:y+2, x-1:x+2] # 3x3 area
            current_angle = area_angle[1,1]
            current_grad_intensity = area_grad_intensity[1,1]
            
            if current_angle == 0:
                if current_grad_intensity > max(area_grad_intensity[1,0], area_grad_intensity[1,2]):
                    keep_mask[y,x] = current_grad_intensity
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 45:
                if current_grad_intensity > max(area_grad_intensity[2,0], area_grad_intensity[0,2]):
                    keep_mask[y,x] = current_grad_intensity
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 90:
                if current_grad_intensity > max(area_grad_intensity[0,1], area_grad_intensity[2,1]):
                    keep_mask[y,x] = current_grad_intensity
                # else:
                #     gradientSum[y,x] = 0
            elif current_angle == 135:
                if current_grad_intensity > max(area_grad_intensity[0,0], area_grad_intensity[2,2]):
                    keep_mask[y,x] = current_grad_intensity
                # else:
                #     gradientSum[y,x] = 0

    return [keep_mask, gradientSum]

def chooseScope(keep_mask, gradientSum, TH, TL):
    outResult = np.zeros(keep_mask.shape, np.uint8)
    img_height = keep_mask.shape[0]
    img_width  = keep_mask.shape[1]
    ###### B4: lay nguong Th Tl
    # TH = 350   # cao
    # TL = 200   # thap
    #TH1 : >TH 
    outResult[(keep_mask>0)*(gradientSum>=TH)] = 255
    #TH2: <TL
    outResult[(keep_mask>0)*(gradientSum<TL)] = 0
    # gradientSum[keep_mask==0] = 0 # loia bo cac diem anh ko dc keep_mask(chon)
    #TH3: TL<...<TH
    for y in range(1, img_height-1):
        for x in range(1, img_width-1):
            if  outResult[y, x] and gradientSum[y, x] > TL and gradientSum[y, x] < TH:
                area_3x3 = gradientSum[y-1:y+2, x-1:x+2] # 3x3 area
                center_area_3x3 = area_3x3[1,1]        # gradientSum[x, y]
                maxarea = max(area_3x3[0,0], area_3x3[0,1], area_3x3[0,2], area_3x3[1,0], area_3x3[1,2], area_3x3[2,0], area_3x3[2,1], area_3x3[2,2] )
                if maxarea >= TH:
                    outResult[y, x] = 255
                else:
                    outResult[y, x] = 0            
    return keep_mask

# B5: rut chinh dac trung
# tong hop 5 buoc
def resultImageDescriptor(image):
    imgGray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    imgS = imgSmoothing(imgGray)
    # rotaImg = rotateImage(imgS, 180)
    gradientSum, gradientAngle  = imgGradient(imgS)
    keep_mask, gradientSum = kindOfPixel(imgS, gradientSum, gradientAngle)
    choSc = chooseScope(keep_mask, gradientSum, 100, 0)    
    return choSc

def rotateImage(image, angle): 
    image_center = tuple(np.array(image.shape[1::-1])/2) 
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0) 
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR) 
    return result 