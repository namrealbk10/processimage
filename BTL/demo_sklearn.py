import numpy as np
import scipy
from sklearn.svm import SVC
from sklearn.datasets import load_digits
from sklearn.linear_model import SGDClassifier
import joblib

X1 = [[1,3], [3,3], [4,0], [3,0], [2, 2]]
y1 = [1, 1, 1, 1, 1]
X2 = [[0,0], [1,1], [1,2], [2,0]]
y2 = [-1, -1, -1, -1]

X3 = [[9,9], [10,10], [20,10], [10,9]]
y3 = ["t", "t", "t", "t"]
X = np.array(X1 + X2 + X3)
y = y1 + y2 + y3

clf = SVC(kernel='linear', C=1E10)
clf.fit(X, y)
x = clf.support_vectors_
y = clf.predict([[9., 8.]])

joblib.dump(clf, 'jobli.pkl')
digits = load_digits()
clf = SGDClassifier().fit(digits.data, digits.target)
print(clf.score(digits.data, digits.target))
# # save the classifier
# with open('my_dumped_classifier.pkl', 'wb') as fid:
#     cPickle.dump(gnb, fid)  


