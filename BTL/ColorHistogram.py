import numpy as np 

# moment cap 1: Mean 
def funMean(data):
    return sum(data)/len(data)

# Moment cấp 2
def funMoment2(data, mean, N):
    sum = 0
    for i in range(len(data)):
        sum += (data[i] - mean)**2
    return np.sqrt(sum/N)

# Moment cấp 3
def funMoment3(data, mean, N):
    sum = 0
    for i in range(len(data)):
        sum += (data[i] - mean)**3
    return (sum/N)**(1/3)
    
def funColorHistogram(imgHSV):
    # get height, width
    height, width = imgHSV.shape[:2]
    numbPixels = height*width
    # init array H
    arrHistH = [0] * 256
    for h in range(height):
        for w in range(width):
            value = imgHSV[h,w][0]
            arrHistH[value] += 1
    # chuan hoa
    for index in range(256):
        arrHistH[index] /= numbPixels

    # init array S
    arrHistS = [0] * 256
    for h in range(height):
        for w in range(width):
            value = imgHSV[h,w][1]
            arrHistS[value] += 1
    # chuan hoa
    for index in range(256):
        arrHistS[index] /= numbPixels

    # init array V
    arrHistV = [0] * 256
    for h in range(height):
        for w in range(width):
            value = imgHSV[h,w][2]
            arrHistV[value] += 1
    # chuan hoa
    for index in range(256):
        arrHistV[index] /= numbPixels
    # Color moments 
    moments = [0] * 9
    # 3 moments cho
    # mỗi kênh H, S, V
    # index 0->2 of H, 3->5 of S, 6->8 of V

        # mean kenh H    
    moments[0] = funMean(arrHistH)
        # mean kenh S
    moments[3] = funMean(arrHistS)
        # mean kenh H    
    moments[6] = funMean(arrHistV)

        # Moment cấp 2 kenh H
    moments[1] = funMoment2(arrHistH, moments[0], len(arrHistH))
        # Moment cấp 2 kenh S
    moments[4] = funMoment2(arrHistS, moments[3], len(arrHistS))
        # Moment cấp 2 kenh V
    moments[7] = funMoment2(arrHistV, moments[6], len(arrHistV))


        # Moment cấp 3 kenh H
    moments[2] = funMoment3(arrHistH, moments[0], len(arrHistH))
        # Moment cấp 3 kenh S
    moments[5] = funMoment3(arrHistS, moments[3], len(arrHistS))
        # Moment cấp 3 kenh V
    moments[8] = funMoment3(arrHistV, moments[6], len(arrHistV))
    return moments
