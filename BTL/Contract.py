import cv2
import ColorHistogram as CH
import ImageDescriptor as IDes
import LBP
import numpy as np
def loadMoreFeatured(nameFruit, numbImgs):
    # outResult = np.zeros(numbImgs)
    outResult = []
    for i in range(numbImgs):
        img = cv2.imread('TrainingDemo/'+ nameFruit +'/'+ str(i) +'_100.jpg')
        featuredColor = CH.funColorHistogram(img)

        imgGray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        featuredLBP = LBP.funGetLBPImage(imgGray)

        LBPImageDescriptor = IDes.collageWithRotate(img)
        featuredHuMent = IDes.fd_hu_moments(LBPImageDescriptor)

        tmpFeatured = list(featuredColor) + list(featuredHuMent) + list(featuredLBP) 
        # tmpFeatured = list(featuredColor) 
        # tmpFeatured = np.concatenate((featuredColor, featuredHuMent, featuredLBP))
        outResult.append(tmpFeatured)
    return outResult

def loadOneFeatured(nameFruit, indexImage):
    outResult = []
    img = cv2.imread('TrainingDemo/'+ nameFruit +'/'+ str(indexImage) +'_100.jpg')
    featuredColor = CH.funColorHistogram(img)

    imgGray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    featuredLBP = LBP.funGetLBPImage(imgGray)

    LBPImageDescriptor = IDes.collageWithRotate(img)
    featuredHuMent = IDes.fd_hu_moments(LBPImageDescriptor)

    tmpFeatured = list(featuredColor) + list(featuredHuMent) + list(featuredLBP) 
    outResult.append(tmpFeatured)
    return outResult
