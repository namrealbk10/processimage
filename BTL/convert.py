import numpy as np

# Convert to unit8
def convertToUint8(data):
    return data.astype(np.uint8)

